<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitor extends CI_Controller {

	
    function __construct(){
            parent::__construct();

            $this->load->model('Monitor_model');
    }
    
    
    function in_bound(){
        
        
        if($this->input->post('mno')){
                
                $mno=$this->input->post('mno');
                $this->data['mno']=$mno;
            }
        
        if($this->input->post('recipient')){

            $recipient=$this->input->post('recipient');
            $this->data['recipient']=$recipient;
        }
            
        if($this->input->post('start')){

            $startdate=$this->input->post('start');
            $this->data['start']=$startdate;
        }

        if($this->input->post('end')){

            $enddate=$this->input->post('end');
            $this->data['end']=$enddate;
        }
            
        if($this->input->post('export')){
            
            $data=$this->Monitor_model->request_inbound_data($recipient,$mno,$startdate,$enddate,null);
        }
       
        $this->data['title']='In Bound Data';
        $this->data['data']=$this->Monitor_model->request_inbound_data($recipient,$mno,$startdate,$enddate,$this->config->item('qry_lmt'));
        $this->data['content']='in_bound';
        $this->load->view('template',$this->data);
    }
    
    function outbound_requests(){
        
        
        if($this->input->post('mno')){
                
                $mno=$this->input->post('mno');
                $this->data['mno']=$mno;
            }
        
        if($this->input->post('recipient')){

            $recipient=$this->input->post('recipient');
            $this->data['recipient']=$recipient;
        }
        
        if($this->input->post('start')){

            $startdate=$this->input->post('start');
            $this->data['start']=$startdate;
        }

        if($this->input->post('end')){

            $enddate=$this->input->post('end');
            $this->data['end']=$enddate;
        }
            
        if($this->input->post('export')){
            
            $data=$this->Monitor_model->request_outbound_data($recipient,$mno,$startdate,$enddate,null);
        }
        
        
        $this->data['title']='Out Bound Data';
        $this->data['data']=$this->Monitor_model->request_outbound_data($recipient,$mno,$startdate,$enddate,$this->config->item('qry_lmt'));
        $this->data['content']='out_bound';
        $this->load->view('template',$this->data);
    }
    
    function in_bound_unknown(){
        
        
        if($this->input->post('mno')){
                
                $mno=$this->input->post('mno');
                $this->data['mno']=$mno;
            }
        
        if($this->input->post('recipient')){

            $recipient=$this->input->post('recipient');
            $this->data['recipient']=$recipient;
        }
        
        if($this->input->post('start')){

            $startdate=$this->input->post('start');
            $this->data['start']=$startdate;
        }

        if($this->input->post('end')){

            $enddate=$this->input->post('end');
            $this->data['end']=$enddate;
        }
            
        if($this->input->post('export')){
            
            $data=$this->Monitor_model->request_inbound_unknown_data($recipient,$mno,$startdate,$enddate,null);
        }
        
        
        $this->data['title']='Unknown Data';
        $this->data['data']=$this->Monitor_model->request_inbound_unknown_data($recipient,$mno,$startdate,$enddate,$this->config->item('qry_lmt'));
        $this->data['content']='unknown';
        $this->load->view('template',$this->data);
    }
    
    function aggregated_report(){
        
        
        if($this->input->post('mno')){
                
                $mno=$this->input->post('mno');
                $this->data['mno']=$mno;
            }
            
        if($this->input->post('startdate')){

            $startdate=$this->input->post('startdate');
            $this->data['startdate']=$startdate;
        }

        if($this->input->post('enddate')){

            $enddate=$this->input->post('enddate');
            $this->data['enddate']=$enddate;
        }
            
        if($this->input->post('export')){
            
            $data=$this->Monitor_model->request_data($mno,$startdate,$enddate,null);
        }
        
        
        $this->data['information']=$this->Monitor_model->request_data($mno,$startdate,$enddate,30);
        $this->load->view('monitor/monitor',$this->data);
    }
    
    function airtel_smsc_session_control(){
        $destination="./logs/airtel_smsc_session_control".date('Y-m-d').'.log';
        //stop smsc connection
        $stop=$this->config->item('stop_smsc_url')."?smsc=".$this->config->item('airtel_smsc_id').'&password='.$this->config->item('admin_password');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$stop);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        $result =(string)trim(curl_exec($ch));

        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);

        echo "result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
        error_log("result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);
        
        sleep(5);
        
        //start smsc connection
        $start=$this->config->item('start_smsc_url')."?smsc=".$this->config->item('airtel_smsc_id').'&password='.$this->config->item('admin_password');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$start);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        $result =(string)trim(curl_exec($ch));

        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);

        echo "result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
        error_log("result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);
        
        sleep(5);
         //status of smsc connection
        $status=$this->config->item('status_smsc_url')."?smsc=".$this->config->item('airtel_smsc_id').'&password='.$this->config->item('admin_password');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$status);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        $result =(string)trim(curl_exec($ch));

        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);

        echo "result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
        error_log("result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);
        
    }
}