<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timwe extends CI_Controller {

    private $responsmsg;
    private $responseurl;
    private $language;
    private $correlationid;
    private $response;
    private $request;
    
    function __construct(){
            parent::__construct();

            $this->load->model('Api_model');
    }
    
    //user opt in notifications handler
    function user_opt_in($partnerRole){
        $destination="./logs/opt_in_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/opt_in_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        
        $this->request =file_get_contents('php://input');
        error_log("Request : $this->request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $source_ip=$this->input->ip_address();
        
        error_log("source : $source_ip ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $headers = apache_request_headers();
        $external_tx_id=$headers['external-tx-id'];
        
        $sve=$this->Api_model->save_incoming_request_timwe($this->request,$source_ip);
            
        if(!$sve){

            //internal server error
            $resp=array(
                        "message"=>"GMOBILE Internal Error",
                        "inError"=>TRUE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"INTERNAL_ERROR",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
                
            $this->response=json_encode($resp);
            error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo trim($this->response);
            exit;
        }
        
        $data= json_decode($this->request);//extract information
        
        
        
        $productId=$data->productId;//product id
        $pricepointId=$data->pricepointId;//price point id
        $mcc=(string)$data->mcc;//mcc
        $mnc=(string)$data->mnc;//mnc
        $entryChannel=(string)$data->entryChannel;//entry channel
        $text=(string)$data->text;//message
        $keyword=explode(" ",$data->text);
        $keyword=$keyword[0];//keyword
        $msisdn=(string)$data->msisdn; //mobile number
        $largeAccount=(string)$data->largeAccount; //shortcode
        $transactionUUID=(string)$data->transactionUUID; //transaction UUID
        
        $this->generateResponse($keyword,$msisdn);
        $this->setRequestid();
        
        $mo=array(
            'productid'=>$productId,
            'pricepointid'=>$pricepointId,
            'mcc'=>$mcc,
            'mnc'=>$mnc,
            'msg'=>$text,
            'msisdn'=>$msisdn,
            'entryChannel'=>$entryChannel,
            'largeaccount'=>$largeAccount,
            'transactionUUID'=>$transactionUUID,
            'correlationid'=>$this->correlationid,
            'externaltxid'=>$external_tx_id,
            'txndate'=>date('y-m-d H:i:s'),
            'responsemsg'=>$this->responsmsg,
            'status'=>100
        );
        
        
        
        $inbound=array(
            'keyword'=>$keyword,
            'text_msg'=>$text,
            'resp_msg'=>$this->responsmsg,
            'source_addr'=>$msisdn,
            'conn_id'=>null,
            'msgtimestsmp'=>null,
            'sms_service_name'=>null,
            'lang'=>$this->language,
            'mno'=>'Tigo',
            'msg_category'=>'101',
            'arrivaltimestamp'=>date('Y-m-d H:i:s'),
            'status'=>'100',
        );
        
            $sve=$this->Api_model->save_timwe_mo($mo,$inbound);
            $resp=array(
                        "message"=>"Notification Received Successfully",
                        "inError"=>FALSE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"SUCCESS",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
            
            if(!$sve){
                
                $resp=array(
                        "message"=>"GMOBILE Internal Error",
                        "inError"=>TRUE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"INTERNAL_ERROR",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
            }
        $this->response=json_encode($resp);
        //
        error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
        echo trim($this->response);
        exit;
    }
    
    //user opt out notifications handler
    function user_opt_out($partnerRole){
        $destination="./logs/opt_out_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/opt_out_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        
        $this->request =file_get_contents('php://input');
        error_log("Request : $this->request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $source_ip=$this->input->ip_address();
        
        error_log("source : $source_ip ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $headers = apache_request_headers();
        $external_tx_id=$headers['external-tx-id'];
        $sve=$this->Api_model->save_incoming_request_timwe($this->request,$source_ip);
            
        if(!$sve){

            //internal server error
            $resp=array(
                        "message"=>"GMOBILE Internal Error",
                        "inError"=>TRUE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"INTERNAL_ERROR",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
                
            $this->response=json_encode($resp);
            error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo trim($this->response);
            exit;
        }
        
        $data= json_decode($this->request);//extract information
        
        
        
        $productId=$data->productId;//product id
        $pricepointId=$data->pricepointId;//price point id
        $mcc=(string)$data->mcc;//mcc
        $mnc=(string)$data->mnc;//mnc
        $entryChannel=(string)$data->entryChannel;//entry channel
        $text=(string)$data->text;//message
        $keyword=explode(" ",$data->text);
        $keyword=$keyword[0];//keyword
        $msisdn=(string)$data->msisdn; //mobile number
        $largeAccount=(string)$data->largeAccount; //shortcode
        $transactionUUID=(string)$data->transactionUUID; //transaction UUID
        
        $this->generateResponse($keyword,$msisdn);
        $this->setRequestid();
        
        $mo=array(
            'productid'=>$productId,
            'pricepointid'=>$pricepointId,
            'mcc'=>$mcc,
            'mnc'=>$mnc,
            'msg'=>$text,
            'msisdn'=>$msisdn,
            'entryChannel'=>$entryChannel,
            'largeaccount'=>$largeAccount,
            'transactionUUID'=>$transactionUUID,
            'correlationid'=>$this->correlationid,
            'externaltxid'=>$external_tx_id,
            'txndate'=>date('y-m-d H:i:s'),
            'responsemsg'=>$this->responsmsg,
            'status'=>100
        );
       
        $inbound=array(
            'keyword'=>$keyword,
            'text_msg'=>$text,
            'resp_msg'=>$this->responsmsg,
            'source_addr'=>$msisdn,
            'conn_id'=>null,
            'msgtimestsmp'=>null,
            'sms_service_name'=>null,
            'lang'=>$this->language,
            'mno'=>'Tigo',
            'msg_category'=>'102',
            'arrivaltimestamp'=>date('Y-m-d H:i:s'),
            'status'=>'100',
        );
       
            $sve=$this->Api_model->save_timwe_mo($mo,$inbound);
            $resp=array(
                        "message"=>"Notification Received Successfully",
                        "inError"=>FALSE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"SUCCESS",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
            
            if(!$sve){
                
                $resp=array(
                        "message"=>"GMOBILE Internal Error",
                        "inError"=>TRUE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"INTERNAL_ERROR",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
            }
            
         $this->response=json_encode($resp);
        //
        error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
        echo trim($this->response);
        exit;
    }
    
    //delivery notifications
    function user_dn($partnerRole){
        $destination="./logs/dn_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/dn_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        
        $this->request =file_get_contents('php://input');
        error_log("Request : $this->request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $source_ip=$this->input->ip_address();
        
        error_log("source : $source_ip ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $headers = apache_request_headers();
        $external_tx_id=$headers['external-tx-id'];
        $sve=$this->Api_model->save_incoming_request_timwe($this->request,$source_ip);
            
        if(!$sve){

            //internal server error
            $resp=array(
                        "message"=>"Partner Internal Error",
                        "inError"=>TRUE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"INTERNAL_ERROR",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
                
            $this->response=json_encode($resp);
            error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo trim($this->response);
            exit;
        }
        
        $data= json_decode($this->request);
        //extract information
        
        $productId=$data->productId;
        $pricepointId=$data->pricepointId;
        $mcc=(string)$data->mcc;
        $mnc=(string)$data->mnc;
        $userIdentifier=(string)$data->userIdentifier;
        $mnoDeliveryCode=(string)$data->mnoDeliveryCode;
        $largeAccount=(string)$data->largeAccount;
        $transactionUUID=(string)$data->transactionUUID;
        
        $this->setRequestid();
        
        $array=array(
            'productid'=>$productId,
            'pricepointid'=>$pricepointId,
            'mcc'=>$mcc,
            'mnc'=>$mnc,
            'userIdentifier'=>$userIdentifier,
            'mnoDeliveryCode'=>$mnoDeliveryCode,
            'largeaccount'=>$largeAccount,
            'transactionUUID'=>$transactionUUID,
            "correlationId"=>$this->correlationid,
            'externaltxid'=>$external_tx_id,
            'txndate'=>date('y-m-d H:i:s'),
        );
        
        $dn=array(
            'dlr_status'=>$mnoDeliveryCode,
            'lastupdate'=>date('Y-m-d H:i:s'),
			'dse_status'=>102,
        );
        
        
        $sve=$this->Api_model->save_timwe_dn($array,$dn,$transactionUUID);
        $resp=array(
                    "message"=>"Notification Received Successfully",
                    "inError"=>FALSE,
                    "requestId"=>"$external_tx_id",
                    "code"=>"SUCCESS",
                    "responseData"=>array(
                                            "transactionUUID"=>"$transactionUUID",
                                            "correlationId"=>"$this->correlationid"
                                        )
                );

        
        if(!$sve){

            $resp=array(
                    "message"=>"Invalid Transaction UUID",
                    "inError"=>TRUE,
                    "requestId"=>"$external_tx_id",
                    "code"=>"INVALID_TRANSACTION",
                    "responseData"=>array(
                                            "transactionUUID"=>"$transactionUUID",
                                            "correlationId"=>"$this->correlationid"
                                        )
                );
        }
        
        
        //response
        $this->response=json_encode($resp);
        error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
        echo trim($this->response);
        exit;
    }
    
    //delivery notifications
    function user_cn($partnerRole){
        $destination="./logs/cn_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/cn_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        
        $this->request =file_get_contents('php://input');
        error_log("Request : $this->request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $source_ip=$this->input->ip_address();
        
        error_log("source : $source_ip ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $headers = apache_request_headers();
        $external_tx_id=$headers['external-tx-id'];
        $sve=$this->Api_model->save_incoming_request_timwe($this->request,$source_ip);
            
        if(!$sve){

            //internal server error
            $resp=array(
                        "message"=>"Partner Internal Error",
                        "inError"=>TRUE,
                        "requestId"=>"$external_tx_id",
                        "code"=>"INTERNAL_ERROR",
                        "responseData"=>array(
                                                "transactionUUID"=>"$transactionUUID",
                                                "correlationId"=>"$this->correlationid"
                                            )
                    );
                
            $this->response=json_encode($resp);
            error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo trim($this->response);
            exit;
        }
        
        $data= json_decode($this->request);
        //extract information
        
        $productId=$data->productId;
        $pricepointId=$data->pricepointId;
        $mcc=(string)$data->mcc;
        $mnc=(string)$data->mnc;
        $userIdentifier=(string)$data->userIdentifier;
        $mnoDeliveryCode=(string)$data->mnoDeliveryCode;
        $largeAccount=(string)$data->largeAccount;
        $transactionUUID=(string)$data->transactionUUID;
        
        $this->setRequestid();
        
        $array=array(
            'productid'=>$productId,
            'pricepointid'=>$pricepointId,
            'mcc'=>$mcc,
            'mnc'=>$mnc,
            'userIdentifier'=>$userIdentifier,
            'mnoDeliveryCode'=>$mnoDeliveryCode,
            'largeaccount'=>$largeAccount,
            'transactionUUID'=>$transactionUUID,
            "correlationId"=>$this->correlationid,
            'externaltxid'=>$external_tx_id,
            'txndate'=>date('y-m-d H:i:s'),
        );
        
        $dn=array(
            'dlr_status'=>$mnoDeliveryCode,
            'lastupdate'=>date('Y-m-d H:i:s'),
            'dse_status'=>102,
        );
        
        $sve=$this->Api_model->save_timwe_cn($array,$dn,$transactionUUID);
        $resp=array(
                    "message"=>"Notification Received Successfully",
                    "inError"=>FALSE,
                    "requestId"=>"$external_tx_id",
                    "code"=>"SUCCESS",
                    "responseData"=>array(
                                            "transactionUUID"=>"$transactionUUID",
                                            "correlationId"=>"$this->correlationid"
                                        )
                );

        
        if(!$sve){

            $resp=array(
                    "message"=>"Invalid Transaction UUID",
                    "inError"=>TRUE,
                    "requestId"=>"$external_tx_id",
                    "code"=>"INVALID_TRANSACTION",
                    "responseData"=>array(
                                            "transactionUUID"=>"$transactionUUID",
                                            "correlationId"=>"$this->correlationid"
                                        )
                );
        }
        
        //response
        $this->response=json_encode($resp);
        error_log("Response : $this->response ".date('Y-m-d H:i:s')."\n", 3, $destination);
        echo trim($this->response);
        exit;
    }
    
    //generate response
    function generateResponse($keyword,$msisdn){
        
        $response=$this->config->item('default_msg');
        $url=$this->config->item('timwe_sub_url');
        $lang="ksw";
        
        if(strtoupper($keyword) == $this->config->item('initial_keyword_en')){
            
            $lang="en";
            $response=$this->config->item('welcome_msg_en');
            
            $check=$this->Api_model->check_account(substr($msisdn,-9),'101','101');
        
            if($check <> null){

                $response=$this->config->item('wrng_welcome_msg_en');
            }
            
        }
        
        if(strtoupper($keyword) == $this->config->item('initial_keyword_ksw')){
            
            $lang="ksw";
            $response=$this->config->item('welcome_msg_ksw');
            
            $check=$this->Api_model->check_account(substr($msisdn,-9),'101','101');
        
            if($check <> null){

                $response=$this->config->item('wrng_welcome_msg_ksw');
            }
           
        }
        
        if(strtoupper($keyword) == $this->config->item('exit_keyword_ksw')){
            
            $lang="ksw";
            $response=$this->config->item('exit_msg_ksw');
            
            $check=$this->Api_model->check_account(substr($msisdn,-9),'101','102');
        
            if($check <> null){

                $response=$this->config->item('wrng_exit_msg_ksw');
            }
            
            $url=$this->config->item('timwe_unsub_url');
        }
        
        if(strtoupper($keyword) == $this->config->item('exit_keyword_en')){
            
            $lang="en";
            $response=$this->config->item('exit_msg_en');
            
            $check=$this->Api_model->check_account(substr($msisdn,-9),'101','102');
        
            if($check <> null){

                    $response=$this->config->item('wrng_exit_msg_en');
            }
            
            $url=$this->config->item('timwe_unsub_url');
        }
      
        $this->responsmsg=$response;
        $this->responsurl=$url;
        $this->language=$lang;
    }
    
    function setRequestid(){
        $str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str=str_shuffle($str);
        
        $packone=substr($str,3, 4);
        $packtwo=substr($str,12, 4);
        $packthree=substr($str,27, 4);
        
        $this->correlationid=$packone."-".$packtwo."-".$packthree."-".date('YmdHis');
    }
}