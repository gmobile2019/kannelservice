<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	
    function __construct(){
            parent::__construct();

            $this->load->model('Api_model');
    }
    
    //airtel registrations...shortcode 15535
    function airtel_reg(){
        $destination="./logs/requests_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/requests__".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $keyword=$this->input->get('mainword');
        $text_msg=$this->input->get('text');
        $source_addr=$this->input->get('sender');
        $conn_id=$this->input->get('smscid');
        $msg_timesatmp=$this->input->get('msgtm');
        $sms_service_name=$this->input->get('smsservice');
        
        error_log("Request : $keyword|$text_msg|$source_addr|$conn_id|$msg_timesatmp|$sms_service_name| ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $lang="ksw";
        $response=$this->config->item('welcome_msg_ksw');
        
        if(strtoupper($keyword) == $this->config->item('initial_keyword_en')){
            
            $lang="en";
            $response=$this->config->item('welcome_msg_en');
        }
        
        $check=$this->Api_model->check_account(substr($source_addr,-9),'101','101');
        
        if($check <> null){
            
            $response=$this->config->item('wrng_welcome_msg_ksw');
            if($lang == "en"){
                $response=$this->config->item('wrng_welcome_msg_en');
            }
        }
        
        $data=array(
            'keyword'=>$keyword,
            'text_msg'=>$text_msg,
            'resp_msg'=>$response,
            'source_addr'=>$source_addr,
            'conn_id'=>$conn_id,
            'msgtimestsmp'=>$msg_timesatmp,
            'sms_service_name'=>$sms_service_name,
            'lang'=>$lang,
            'mno'=>'Airtel',
            'msg_category'=>'101',
            'arrivaltimestamp'=>date('Y-m-d H:i:s'),
            'status'=>'100',
        );
        
        while(true){
            $sve=$this->Api_model->save_incoming_request($data);
        
            if($sve){
                error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
                echo trim($response);
                exit;
            }
        }
        
    }
    
    //airtel unregistrations...shortcode 15535
    function airtel_exit(){
        $destination="./logs/requests_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/requests__".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $keyword=$this->input->get('mainword');
        $text_msg=$this->input->get('text');
        $source_addr=$this->input->get('sender');
        $conn_id=$this->input->get('smscid');
        $msg_timesatmp=$this->input->get('msgtm');
        $sms_service_name=$this->input->get('smsservice');
        
        error_log("Request : $keyword|$text_msg|$source_addr|$conn_id|$msg_timesatmp|$sms_service_name| ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $lang="ksw";
        $response=$this->config->item('exit_msg_ksw');
        
        if(strtoupper($keyword) == $this->config->item('exit_keyword_en')){
            
            $lang="en";
            $response=$this->config->item('exit_msg_en');
        }
        
        $check=$this->Api_model->check_account(substr($source_addr,-9),'101','102');
        
        if($check <> null){
            
            $response=$this->config->item('wrng_exit_msg_ksw');
            if($lang == "en"){
                $response=$this->config->item('wrng_exit_msg_en');
            }
        }
        
        $data=array(
            'keyword'=>$keyword,
            'text_msg'=>$text_msg,
            'resp_msg'=>$response,
            'source_addr'=>$source_addr,
            'conn_id'=>$conn_id,
            'msgtimestsmp'=>$msg_timesatmp,
            'sms_service_name'=>$sms_service_name,
            'lang'=>$lang,
            'mno'=>'Airtel',
            'arrivaltimestamp'=>date('Y-m-d H:i:s'),
            'msg_category'=>'102',
            'status'=>'100',
        );
        
        while(true){
            $sve=$this->Api_model->save_incoming_request($data);
        
            if($sve){
                error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
                echo trim($response);
                exit;
            }
        }
    }
    
    //default service
    function default_sms_service(){
        $destination="./logs/requests_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/requests__".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $keyword=$this->input->get('mainword');
        $text_msg=$this->input->get('text');
        $source_addr=$this->input->get('sender');
        $conn_id=$this->input->get('smscid');
        $msg_timesatmp=$this->input->get('msgtm');
        $sms_service_name=$this->input->get('smsservice');
        
        error_log("Request : $keyword|$text_msg|$source_addr|$conn_id|$msg_timesatmp|$sms_service_name| ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        
        $response=$this->config->item('default_msg');
        
        $data=array(
            'keyword'=>$keyword,
            'text_msg'=>$text_msg,
            'resp_msg'=>$response,
            'source_addr'=>$source_addr,
            'conn_id'=>$conn_id,
            'msgtimestsmp'=>$msg_timesatmp,
            'sms_service_name'=>$sms_service_name,
            'lang'=>'ksw',
            'mno'=>'Airtel',
            'arrivaltimestamp'=>date('Y-m-d H:i:s'),
            'msg_category'=>'102',
            'status'=>'100',
        );
        
        while(true){
            $sve=$this->Api_model->save_unknown($data);
            
            if($sve){
            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo trim($response);
            exit;
            }
        }
    }
    
    //reply
    function send_response_airtel($recipient,$message){
        
        $sendsmsurl=$this->config->item('send_resp_url')."?username=".$this->config->item('kannel_101_usr').'&password='.$this->config->item('kannel_101_pwd')."&smsc=".$this->config->item('101_mo_smsc')."&from=15535&to=".$recipient."&text=".urlencode($message);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$sendsmsurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result =(string)trim(curl_exec($ch));

        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);
        
        return $result;
    }
    
    //kannel outbound messages
    function kannel_outbound(){
      $destination="./logs/outbound_".date('Y-m-d').'.log'; 
        $request =file_get_contents('php://input');
        
        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 10485760){
                rename($destination,"./logs/outbound_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $remote_address=$_SERVER['REMOTE_ADDR'];
        
        error_log("Client IP address : $remote_address ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $errorcode=$this->process_outbound_request($request);
        $details=$this->Api_model->get_response_details($errorcode);

        $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";

        error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
        ob_end_clean();

        echo $response;
        exit;
     
    }
    
    //kannel outbound messages
    function process_outbound_request($request){
        $destination="./logs/outbound_".date('Y-m-d').'.log'; 
        $entityBody =file_get_contents('php://input');
        
        $xml=$this->validate_xml($request);
        
        
        if(!$xml){
            //invalid xml request
            return 102;
        }
        //verify access token
        $access=$this->Api_model->check_access_credentials((string)trim($xml->username),$this->hash_password((string)trim($xml->password)));
        
        if(!$access){
            //invalid access credentials
            error_log("invalid access credentials ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 103;
        }
        
        $sendercode=(string)$xml->data->sendercode;
        $recipient=(string)$xml->data->recipient;
        $msg=(string)$xml->data->message;
        $msgid=(string)$xml->data->messageid;
        
        error_log("$sendercode|$recipient|$msgid ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $array=array(
            'message'=>$msg,
            'dse_messageid'=>$msgid,
            'dse_timestamp'=>date('Y-m-d H:i:s'),
            'dse_recipient'=>$recipient,
            'dse_status'=>100,
            'sendercode'=>$sendercode
        );
        
        $sve=$this->Api_model->save_outbound_request($array);
        if(!$sve){
            
            //internal server error
            error_log("Database Error : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 105;
        }
        
        error_log("successfull ".date('Y-m-d H:i:s')."\n", 3, $destination);
        return 100;
    }
    
    //kannel outbound messages
    function validate_xml($request){
        $destination="./logs/outbound_".date('Y-m-d').'.log';
        libxml_use_internal_errors(true);
       
        $xml = simplexml_load_string($request);
      
        if (!$xml) {
            error_log("Xml invalid ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $errors = libxml_get_errors();
            
            foreach ($errors as $error) {
                
                error_log("Error : $error->message ".date('Y-m-d H:i:s')."\n", 3, $destination);
               
            }

            libxml_clear_errors();
            return FALSE;
        }
        error_log("Xml Valid ".date('Y-m-d H:i:s')."\n", 3, $destination);
        return $xml;
    }
    
    //kannel outbound messages
    function hash_password($password){
        
        return md5($password);
    }
    
    //dlr responses...shortcode 15535
    function dlr_notify(){
        $destination="./logs/dlr_inbound_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/dlr_inbound_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $msgid=$this->input->get('msgid');
        $dlrval=$this->input->get('dlrval');
        $dlrmsgid=$this->input->get('dlrmsgid');
        $dlrtime=$this->input->get('dlrtime');
        
        error_log("$msgid|$dlrval|$dlrmsgid|$dlrtime ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $dse_status=101;
        if(in_array($dlrval,$this->config->item('final_dlr_vls'))){
            
            $dse_status=102;
        }
        
        $data=array(
            'dlr_messageid'=>$dlrmsgid,
            'dlr_timestamp'=>$dlrtime,
            'dlr_status'=>$dlrval,
            'lastupdate'=>date('Y-m-d H:i:s'),
            'dse_status'=>$dse_status,
        );
        
        $this->Api_model->update_dlr_smsc($data,$msgid);
        exit;
    }
}