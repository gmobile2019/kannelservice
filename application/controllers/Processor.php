<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Processor extends CI_Controller {

	private $externaltxid;
    private $authenticationkey;
	
    function __construct(){
            parent::__construct();
			ini_set('max_execution_time',0);
            $this->load->model('Processor_model');
    }
    
    //inbound requests
    function pending_inbound_requests(){
        $lock =file_exists('./locks/pd_inb_req.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/pd_inb_req.lock','w'); 
        }
        
        $destination="./logs/pd_inbound_kannel_requests_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/pd_inbound_kannel_requests_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        if($this->config->item('app_debug')){
            error_log("start : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "start : ".date('Y-m-d H:i:s')."\n";
        }
            
        $t=0;
        while($t++ < $this->config->item('script_lp_cnt')){
           
            $reqs=$this->Processor_model->pd_inbound_reqs();

            if($reqs == null){

                if($this->config->item('app_debug')){
                    error_log("no pds ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    echo "no pds ".date('Y-m-d H:i:s')."\n";
                }
                continue;
            }
			
			$resp_arr=array();
            $update_string="";
            foreach($reqs as $key=>$value){


                error_log("$value->source_addr|$value->lang|$value->mno|$value->msg_category ".date('Y-m-d H:i:s')."\n", 3, $destination);
                echo "$value->source_addr|$value->lang|$value->mno||$value->msg_category ".date('Y-m-d H:i:s')."\n";

                $token=$this->request_token();

                if($token == null || $token == ""){

                    if($this->config->item('app_debug')){
                        error_log("no token generated \n", 3, $destination);
                        echo "no token generated \n";
                    }
                    continue;
                }

                $resp=$this->customer_configuration($token,$value->source_addr,$value->lang,$value->mno,$value->msg_category,$value->resp_msg);
                
                $status='102';
                if($resp){

                    $status='101';
                }
				
				 $resp_arr[]=array(
                    'id'=>$value->id,
                    'status'=>$status,
                );
                
                $update_string .="$value->id|$status&&";
			}
			
			if($resp_arr <> null){
                
                error_log("$update_string \n", 3, $destination);
                echo "$update_string \n";
                
                while(true){
                    $sve=$this->Processor_model->update_inbound_batch_data($resp_arr);
                    
                    if($sve){
                        break;
                    }
                }
            }
            sleep(2);
        }
		
        if($this->config->item('app_debug')){
            error_log("end : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "end : ".date('Y-m-d H:i:s')."\n";
        }
        fclose($lk);
        unlink('./locks/pd_inb_req.lock');
    }
    
    //outbound requests
    function pending_outbound_requests(){
        $lock =file_exists('./locks/pd_outb_req.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/pd_outb_req.lock','w'); 
        }
        
        $destination="./logs/pd_outbound_kannel_requests_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/pd_outbound_kannel_requests_".date('His')."_".date('Y-m-d').'.log');
            }
        }
            
		if($this->config->item('app_debug')){
            error_log("start : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "start : ".date('Y-m-d H:i:s')."\n";
        }
            
        $t=0;
        while($t++ < $this->config->item('script_lp_cnt')){
            
            $reqs=$this->Processor_model->pd_outbound_reqs();

            if($reqs == null){
	
				if($this->config->item('app_debug')){
                    error_log("no pds \n", 3, $destination);
                    echo "no pds \n";
                }
                continue;
            }
			$arr=array();
            $ds_ids=NULL;
            foreach($reqs as $key=>$value){

                error_log("$value->sendercode|$value->dse_recipient|$value->dse_messageid \n", 3, $destination);
                echo "$value->sendercode|$value->dse_recipient|$value->dse_messageid \n";

                //generate request data
                $org_dlr=$this->config->item('dlr_url')."?msgid=".$value->dse_messageid."&dlrval=%d&dlrmsgid=%F&dlrtime=%t";
                $dlrurl=urlencode($org_dlr);

                $sendsmsurl=$this->config->item('send_sms_url')."?username=".$this->config->item('kannel_'.$value->sendercode.'_usr').'&password='.$this->config->item('kannel_'.$value->sendercode.'_pwd')."&smsc=".$this->config->item('101_mt_smsc')."&from=15535&to=".$value->dse_recipient."&text=".urlencode($value->message)."&dlr-mask=31&dlr-url=".$dlrurl;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$sendsmsurl);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result =(string)trim(curl_exec($ch));

                $errno=curl_errno($ch);
                $error=curl_error($ch);

                curl_close($ch);

                if($this->config->item('app_debug')){
                    echo "result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
                    error_log("result : $result , error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);
                }

                if($errno == 0 && ($result == "0: Accepted for delivery" || $result == "3: Queued for later delivery")){
                    
                    $arr[]=array(
                        'dlr_url'=>$org_dlr,
                        'dse_messageid'=>$value->dse_messageid,
                        'kannel_status'=>$result,
                        'dse_status'=>101
                        );
                    
                   $ds_ids .=$value->dse_messageid."|";
                     
                }
            }
			
			if($arr <> null){
                
                 while(true){
                     $sve=$this->Processor_model->update_outbound_batch_data($arr,'outbound');
                     
                     if($sve){
                         
                         echo "success : $ds_ids ".date('Y-m-d H:i:s')."\n";
                         error_log("success : $ds_ids ".date('Y-m-d H:i:s')."\n",3, $destination);
                         break;
                     }
                 }
                 
            }
			
            sleep(2);
        }
		
        if($this->config->item('app_debug')){
            
            error_log("end : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "end : ".date('Y-m-d H:i:s')."\n";
        }
		
        fclose($lk);
        unlink('./locks/pd_outb_req.lock');
    }
    
    //dlr responses
    function pending_dlr_resp(){
        
        $lock =file_exists('./locks/pd_dlr_resp.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/pd_dlr_resp.lock','w'); 
        }
        
        $destination="./logs/pd_dlr_resps_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/pd_dlr_resps_".date('His')."_".date('Y-m-d').'.log');
            }
        }
            
        if($this->config->item('app_debug')){
            error_log("start : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "start : ".date('Y-m-d H:i:s')."\n";
        }
        
        $t=0;
        while($t++ < $this->config->item('script_lp_cnt')){
            
            $resps=$this->Processor_model->pd_dlr_resps();

            if($resps == null){

                if($this->config->item('app_debug')){
                    error_log("no pds \n", 3, $destination);
                    echo "no pds \n";
                }
                continue;
            }
			$arr=array();
            $ds_ids=NULL;
            foreach($resps as $key=>$value){

                $dlr_status=102;
                if(in_array($value->dlr_status, $this->config->item('success_dlr_vals'))){

                    $dlr_status=101;
                }

                error_log("$value->dlr_status|$value->dse_recipient|$value->dse_messageid ".date('Y-m-d H:i:s')."\n", 3, $destination);
                echo "$value->dlr_status|$value->dse_recipient|$value->dse_messageid ".date('Y-m-d H:i:s')."\n";

                $token=$this->request_token_dlr();

                if($token == null){

                    if($this->config->item('app_debug')){
                        error_log("no token generated ".date('Y-m-d H:i:s')."\n", 3, $destination);
                        echo "no token generated ".date('Y-m-d H:i:s')."\n";
                    }

                    continue;
                }

                $dlrreq='<?xml version="1.0" encoding="UTF-8"?>
                <request><data method="dlr_report" username="'.$this->config->item('dse_cust_usr').'" password="'.$this->config->item('dse_cust_pwd').'"><token>'.$token.'</token><accountno>'.$value->dse_recipient.'</accountno><lang></lang><mno></mno><status>'.$dlr_status.'</status><messageid>'.$value->dse_messageid.'</messageid></data></request>';

                $resp=$this->send_dlr($dlrreq);

                if($resp){

                    $arr[]=array(
                        'dse_messageid'=>$value->dse_messageid,
                        'dse_status'=>103
                        );
                    
                   $ds_ids .=$value->dse_messageid."|";
                }
            }
			
			if($arr <> null){
                
                 while(true){
                     $sve=$this->Processor_model->update_outbound_batch_data($arr,'dlr_report');
                     
                     if($sve){
                         
                         echo "success : $ds_ids ".date('Y-m-d H:i:s')."\n";
                         error_log("success : $ds_ids ".date('Y-m-d H:i:s')."\n",3, $destination);
                         break;
                     }
                 }
                 
            }
            sleep(2);
        }
		
        if($this->config->item('app_debug')){
            error_log("end : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "end : ".date('Y-m-d H:i:s')."\n";
        }
		
        fclose($lk);
        unlink('./locks/pd_dlr_resp.lock');
    }
    
	//timwe chargable requests
    function timwe_chargable_requests(){
        $lock =file_exists('./locks/timwe_chrgble_req.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/timwe_chrgble_req.lock','w'); 
        }
        
        $destination="./logs/pd_timwe_mt_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/pd_timwe_mt".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        if($this->config->item('app_debug')){
            error_log("start : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "start : ".date('Y-m-d H:i:s')."\n";
        }
           
        $t=0;
        while($t++ < $this->config->item('script_lp_cnt')){
            
            $reqs=$this->Processor_model->pd_timwe_chargable_reqs();

            if($reqs == null){
                
                if($this->config->item('app_debug')){
                    error_log("no pds \n", 3, $destination);
                    echo "no pds \n";
                }
                
                continue;
            }
            $arr=array();
            $ds_ids=NULL;
            foreach($reqs as $key=>$value){

                error_log("$value->sendercode|$value->dse_recipient|$value->dse_messageid|chargable \n", 3, $destination);
                echo "$value->sendercode|$value->dse_recipient|$value->dse_messageid|chargable \n";

                $data=array(
                    'productId'=>"".$this->config->item('timwe_product_id')."",
                    'pricepointId'=>"".$this->config->item('timwe_chargable_pricepoint_id')."",
                    'mcc'=>"".$this->config->item('timwe_mcc')."",
                    'mnc'=>"".$this->config->item('timwe_mnc')."",
                    'msisdn'=>"$value->dse_recipient",
                    'largeAccount'=>"".$this->config->item('dse_shortcode')."",
                    'text'=>"".$value->message."",
                    'sendDate'=>"".date('Y-m-d H:i:s')."",
                    'timezone'=>"Africa/Dar es salaam",
                    'priority'=>"MEDIUM",
                    'context'=>"STATELESS",
                );
                
                $transactionUUID=$this->send_timwe_mt($data,'chargable');
                
                if($transactionUUID){
                    
                    $arr[]=array(
                        'dse_messageid'=>$value->dse_messageid,
                        'kannel_status'=>'Accepted for delivery',
                        'dlr_messageid'=>$transactionUUID,
                        'dse_status'=>101
                        );
                    
                   $ds_ids .=$value->dse_messageid."|";
                     
                }
            }
            
            if($arr <> null){
                
                 while(true){
                     $sve=$this->Processor_model->update_timwe_batch_data($arr,'chargable');
                     
                     if($sve){
                         
                         echo "success : $ds_ids ".date('Y-m-d H:i:s')."\n";
                         error_log("success : $ds_ids ".date('Y-m-d H:i:s')."\n",3, $destination);
                         break;
                     }
                 }
                 
            }
            
            sleep(2);
        }
        
        if($this->config->item('app_debug')){
            
            error_log("end : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "end : ".date('Y-m-d H:i:s')."\n";
        }
        
        fclose($lk);
        unlink('./locks/timwe_chrgble_req.lock');
    }
    
    //timwe chargable requests
    function timwe_unchargable_requests(){
        $lock =file_exists('./locks/timwe_unchrgble_req.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/timwe_unchrgble_req.lock','w'); 
        }
        
        $destination="./logs/pd_timwe_mt_".date('Y-m-d').'.log'; 

        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 1048576){
                rename($destination,"./logs/pd_timwe_mt".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        if($this->config->item('app_debug')){
            error_log("start : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "start : ".date('Y-m-d H:i:s')."\n";
        }
           
        $t=0;
        while($t++ < $this->config->item('script_lp_cnt')){
            
            $reqs=$this->Processor_model->pd_timwe_unchargable_reqs();

            if($reqs == null){
                
                if($this->config->item('app_debug')){
                    error_log("no pds \n", 3, $destination);
                    echo "no pds \n";
                }
                
                continue;
            }
            $arr=array();
            $ds_ids=NULL;
            foreach($reqs as $key=>$value){

                error_log("$value->msisdn|$value->transactionUUID|unchargable \n", 3, $destination);
                echo "$value->dse_recipient|$value->transactionUUID|unchargable \n";

                $data=array(
                    'productId'=>"".$this->config->item('timwe_product_id')."",
                    'pricepointId'=>"".$this->config->item('timwe_unchargable_pricepoint_id')."",
                    'mcc'=>"".$this->config->item('timwe_mcc')."",
                    'mnc'=>"".$this->config->item('timwe_mnc')."",
                    'msisdn'=>"$value->msisdn",
                    'largeAccount'=>"".$this->config->item('dse_shortcode')."",
                    'text'=>"$value->responsemsg",
                    'moTransactionUUID'=>"$value->transactionUUID",
                    'sendDate'=>"".date('Y-m-d H:i:s')."",
                    'timezone'=>"Africa/Dar es salaam",
                    'priority'=>"MEDIUM",
                    'context'=>"STATELESS",
                );
                
                $transactionUUID=$this->send_timwe_mt($data,'unchargable');
                
                if($transactionUUID){
                    
                    $arr[]=array(
                        'transactionUUID'=>$value->transactionUUID,
						'dlr_messageid'=>$transactionUUID,
                        );
                    
                   $ds_ids .=$value->transactionUUID."|";
                     
                }
            }
            
            if($arr <> null){
                
                 while(true){
                     $sve=$this->Processor_model->update_timwe_batch_data($arr,'unchargable');
                     
                     if($sve){
                         
                         echo "success : $ds_ids ".date('Y-m-d H:i:s')."\n";
                         error_log("success : $ds_ids ".date('Y-m-d H:i:s')."\n",3, $destination);
                         break;
                     }
                 }
                 
            }
            
            sleep(2);
        }
        
        if($this->config->item('app_debug')){
            
            error_log("end : ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "end : ".date('Y-m-d H:i:s')."\n";
        }
        
        fclose($lk);
        unlink('./locks/timwe_unchrgble_req.lock');
    }
	
    //dlr responses
    function send_dlr($request){
        $destination="./logs/pd_dlr_resps_".date('Y-m-d').'.log';
        
                $ch =curl_init($this->config->item('dse_customer_url'));

                if($this->config->item('app_debug')){
                    error_log("Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    echo "Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n";

                    error_log("request : ".$request." ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    echo "request : ".$request." ".date('Y-m-d H:i:s')."\n";
                }

                $header= array('Content-type: text/xml','Content-Length: ' . strlen($request));

                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch,CURLOPT_TIMEOUT,5);
                $response=curl_exec($ch);
                
                $errno=curl_errno($ch);
                $error=curl_error($ch);

                curl_close($ch);

                if($this->config->item('app_debug')){
                    echo "error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
                    error_log("error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);

                    error_log("response : ".$response." ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    echo "response : ".$response." ".date('Y-m-d H:i:s')."\n";
                } 
                
                if($errno <> 0){
                    
                    return FALSE;
                }
                
                if($response){
                    
                    $xml=simplexml_load_string($response);
                    
                    if((int)$xml->status == 'success'){
                        
                        return TRUE;
                    }
                    
                    return FALSE;
                }
    }
    
    //customer configuration
    function request_token(){
        $destination="./logs/pd_inbound_kannel_requests_".date('Y-m-d').'.log';
        
        $ch =curl_init($this->config->item('dse_customer_url'));

        $request='<?xml version="1.0" encoding="UTF-8"?>
            <request><data method="token_request" username="'.$this->config->item('dse_cust_usr').'" password="'.$this->config->item('dse_cust_pwd').'"><token></token><accountno></accountno><alerttype></alerttype><email></email><message></message></data></request>';

        if($this->config->item('app_debug')){
            error_log("Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n";
            
            error_log("request : ".$request." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "request : ".$request." ".date('Y-m-d H:i:s')."\n";
        }

        $header= array('Content-type: text/xml','Content-Length: ' . strlen($request));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        $response=curl_exec($ch);
        
        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);

        if($this->config->item('app_debug')){
            echo "error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
            error_log("error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);

            error_log("response : ".$response." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "response : ".$response." ".date('Y-m-d H:i:s')."\n";
        } 

        if($errno <> 0){

            return FALSE;
        }

        if($response){

            $xml=simplexml_load_string($response);
            return $xml->token;
        }
    }
    
    //dlr reporting
    function request_token_dlr(){
        $destination="./logs/pd_dlr_resps_".date('Y-m-d').'.log';
        
        $ch =curl_init($this->config->item('dse_customer_url'));
		
		$request='<?xml version="1.0" encoding="UTF-8"?>
            <request><data method="token_request" username="'.$this->config->item('dse_cust_usr').'" password="'.$this->config->item('dse_cust_pwd').'"><token></token><accountno></accountno><alerttype></alerttype><email></email><message></message></data></request>';
			
        if($this->config->item('app_debug')){
            error_log("Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n";

            error_log("request : ".$request." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "request : ".$request." ".date('Y-m-d H:i:s')."\n"; 
        } 

        $header= array('Content-type: text/xml','Content-Length: ' . strlen($request));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        $response=curl_exec($ch);
        
        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);

        if($this->config->item('app_debug')){
            echo "error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
            error_log("error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);

            error_log("response : ".$response." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "response : ".$response." ".date('Y-m-d H:i:s')."\n";
        } 

        if($errno <> 0){

            return FALSE;
        }

        if($response){

            $xml=simplexml_load_string($response);
            return $xml->token;
        }
    }
    
    //customer configuration
    function customer_configuration($token,$source_addr,$lang,$mno,$status,$resp_msg){
        $destination="./logs/pd_inbound_kannel_requests_".date('Y-m-d').'.log';
        
        $ch =curl_init($this->config->item('dse_customer_url'));

        $request='<?xml version="1.0" encoding="UTF-8"?>
            <request><data method="subscription" username="'.$this->config->item('dse_cust_usr').'" password="'.$this->config->item('dse_cust_pwd').'"><token>'.$token.'</token><accountno>'.$source_addr.'</accountno><lang>'.$lang.'</lang><mno>'.$mno.'</mno><status>'.$status.'</status><msg>'.$resp_msg.'</msg></data></request>';

        if($this->config->item('app_debug')){
            error_log("Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "Url : ".$this->config->item('dse_customer_url')." ".date('Y-m-d H:i:s')."\n";

            error_log("request : ".$request." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "request : ".$request." ".date('Y-m-d H:i:s')."\n"; 
        } 

        $header= array('Content-type: text/xml','Content-Length: ' . strlen($request));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        $response=curl_exec($ch);

        $errno=curl_errno($ch);
        $error=curl_error($ch);

        curl_close($ch);
                
        if($this->config->item('app_debug')){
             echo "error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n";
            error_log("error no : $errno , error : $error ".date('Y-m-d H:i:s')."\n",3, $destination);

            error_log("response : ".$response." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "response : ".$response." ".date('Y-m-d H:i:s')."\n"; 
        }

        if($errno <> 0){

            return FALSE;
        } 

        if($response){

            $xml=simplexml_load_string($response);
            
            if($xml->status == 'success'){
                
                return TRUE;
            }
            
            return FALSE;
        }
    }

	//send timwe mt
    function send_timwe_mt($data,$category){
        $destination="./logs/pd_timwe_mt_".date('Y-m-d').'.log';
        
        
        $data =json_encode($data);
                
        if($this->config->item('app_debug')){

           error_log("request : ".$data." |$category|$prid ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "request : ".$data." |$category|$prid ".date('Y-m-d H:i:s')."\n"; 
        }
                 
        $strToEncrypt=$this->config->item('gmobile_service_id')."#" .round(microtime(true) * 1000);         
        $this->setAuthenticationKey($strToEncrypt);
        
        $header= array(
                    'Content-Type: application/json',
                    'apikey: '.$this->config->item('timwe_api_key'),
                    'authentication: '.$this->authenticationkey,
                );
		
        
		$url=$this->config->item('timwe_base_url')."sms/mt/".$this->config->item('timwe_partner_role');
        if($this->config->item('app_debug')){

            error_log("URL : ".$url."\n", 3, $destination);
            echo "URL : ".$url."\n"; 
        }
        
        $logheader=var_export($header,true);
        if($this->config->item('app_debug')){

            error_log("HEADER : ".$logheader."\n", 3, $destination);
            echo "HEADER : ".$logheader."\n"; 
        }
        
        
        $ch=curl_init($url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        $response=curl_exec($ch);

        if($this->config->item('app_debug')){

            error_log("connection details : ".curl_errno($ch)."(".curl_error($ch).")|$category ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "connection details : ".curl_errno($ch)."(".curl_error($ch).")|$category ".date('Y-m-d H:i:s')."\n";
            error_log("$response|$category ".date('Y-m-d H:i:s')."\n", 3, $destination);
            echo "$response|$category ".date('Y-m-d H:i:s')."\n";
        }
        
        $response=json_decode($response);
        
        if((string)$response->code=="SUCCESS"){
            
            return (string)$response->responseData->transactionUUID;
        }
        return FALSE;
    }
    
    function setExternalTxId(){
        $str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str=str_shuffle($str);
        
        $this->externaltxid=substr($str,0,6).date('YmdHis');
    }
    
    function setAuthenticationKey($strToEncrypt){
        
        $strToEncrypt = $this->pkcs5_pad($strToEncrypt, 16);
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128,$this->config->item('timwe_psk'),$strToEncrypt, MCRYPT_MODE_ECB);
        $ciphertext_base64 = base64_encode($ciphertext);
        
        $this->authenticationkey=$ciphertext_base64;
    }
	
	function pkcs5_pad($text,$blocksize){
        
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }
}