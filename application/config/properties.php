<?php

/*
 * request sample
    <?xml version="1.0" encoding="UTF-8"?>
    <request>
    <username></username>
    <password></password>
    <data>
        <sendercode></sendercode>
        <recipient></recipient>
        <message></message>
        <messageid></messageid>
    </data>
    </request>
 * 
 * response sample
    <?xml version="1.0" encoding="UTF-8"?>
    <response>
        <code></code>
        <status><status>
        <description></description>
    </response>
    
 * response codes
 */

$config['welcome_msg_en']='You have successfully registered to the DSE sms Alert Notification System. To unsubscribe send the word "STOP" to 15535. Thank you';

$config['wrng_welcome_msg_en']='Dear subscriber you are already registered to the DSE sms Alert Notification System. To unsubscribe send the word "STOP" to 15535. Thank you';

$config['welcome_msg_ksw']='Umefanikiwa kujiunga na mfumo wa taarifa za DSE kwa njia ya ujumbe mfupi.Kujitoa tuma neno "ONDOA" kwenda 15535. Asante';

$config['wrng_welcome_msg_ksw']='Mpendwa mteja akaunti yako ya kupokea taarifa za DSE kwa njia ya ujumbe mfupi bado ipo hai. Kujitoa tuma neno "ONDOA" kwenda 15535. Asante';

$config['exit_msg_ksw']='Umefanikiwa kujitoa kutoka kwenye mfumo wa taarifa za DSE kwa njia ya ujumbe mfupi.Kujiunga tuma neno "SAJILI" kwenda 15535. Asante';

$config['wrng_exit_msg_ksw']='Kujiunga na huduma ya DSE sms Alerts Notification tafadhali tuma neno "SAJILI" kwenda namba 15535. Asante';

$config['exit_msg_en']='You have successfully unregistered from DSE sms Alert Notification System. To subscribe send the word "REGISTER" to 15535. Thank you';

$config['wrng_exit_msg_en']='To subscribe to DSE sms Alert Notification System please send the word "REGISTER" to 15535. Thank you';

$config['default_msg']='Tafadhali tuma neno "SAJILI" au "REGISTER" kwenda 15535 kujiunga na mfumo wa DSE sms Alerts Notification System. Kujitoa tuma neno "ONDOA" au "STOP" kwenda 15535';

$config['initial_keyword_ksw']='SAJILI';

$config['initial_keyword_en']='REGISTER';

$config['exit_keyword_en']='STOP';

$config['exit_keyword_ksw']='ONDOA';

$config['kannel_101_usr']= 'airtel';

$config['kannel_101_pwd']= 'airtelgmobile';

$config['101_mo_smsc']= 'ADMO';

$config['101_mt_smsc']= 'ADMT';

$config['salt']='gmobile2016';

$config['dse_cust_usr']='dse';

$config['dse_cust_pwd']='123456';

$config['dlr_url']="http://localhost:8181/KannelService/index.php/Api/dlr_notify";

$config['send_sms_url']="http://localhost:13013/cgi-bin/sendsms";

$config['send_resp_url']="http://localhost:13013/cgi-bin/sendsms";

$config['dse_customer_url']="http://localhost:8181/dse/index.php/Api";

$config['final_dlr_vls']=array(1,2,16);

$config['success_dlr_vals']=array('1','DELIVERED');

$config['script_lp_cnt']=1000;

$config['stop_smsc_url']='http://localhost:13000/stop-smsc';

$config['start_smsc_url']='http://localhost:13000/start-smsc';

$config['status_smsc_url']='http://localhost:13000/status';

$config['airtel_smsc_id']='AAD2017';

$config['admin_password']='gmobile';

$config['qry_lmt']='15';

$config['app_debug']=FALSE;

$config['kannelsendercodes']="'101'";

$config['timwesendercode']="'102'";

$config['timwe_partner_role']=1009;

$config['timwe_product_id']=3177;

$config['gmobile_service_id']=1094;

$config['dse_shortcode']=15535;

$config['timwe_mnc']='02';

$config['timwe_mcc']='640';

$config['timwe_entry_channel']="SMS";

$config['timwe_api_key']="7a9df8c5aa2048d68eb570c282cfc080";

$config['timwe_psk']="Z2iOXh5sIIcxztWz";

$config['timwe_chargable_pricepoint_id']=2375;

$config['timwe_unchargable_pricepoint_id']=2177;

$config['timwe_base_url']="https://tigo.timwe.com/tz/ma/api/external/v1/";