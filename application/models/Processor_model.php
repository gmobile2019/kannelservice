<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Processor_model extends CI_Model
{
    private $kannel;
    
    public function __construct()
	{
		parent::__construct();
		$this->kannel=$this->load->database('kannel',TRUE);//load main database configuration
        }
        
    function pd_outbound_reqs(){
        
        return $this->kannel->query("SELECT message,dse_messageid,dse_timestamp,dse_recipient,sendercode FROM tbl_shortcode_outbound WHERE dse_status='100' AND sendercode IN (".$this->config->item('kannelsendercodes').")ORDER BY id ASC LIMIT 0,100")->result();
    }
    
	function pd_timwe_chargable_reqs(){
        
        return $this->kannel->query("SELECT message,dse_messageid,dse_timestamp,dse_recipient,sendercode FROM tbl_shortcode_outbound WHERE dse_status='100' AND sendercode IN (".$this->config->item('timwesendercode').") ORDER BY id ASC LIMIT 0,100")->result();
    }
    
    function pd_timwe_unchargable_reqs(){
        
        return $this->kannel->query("SELECT responsemsg,transactionUUID,msisdn FROM timwe_mo WHERE status='100' ORDER BY id ASC LIMIT 0,100")->result();
    }
	
    function pd_inbound_reqs(){
        
        return $this->kannel->query("SELECT id,source_addr,lang,mno,msg_category,resp_msg FROM tbl_shortcode_inbound WHERE status='100' ORDER BY id ASC LIMIT 0,100")->result();
    }
    
    function pd_dlr_resps(){
        
        return $this->kannel->query("SELECT dse_messageid,dlr_status,dse_recipient FROM tbl_shortcode_outbound WHERE dse_status='102' LIMIT 0,100")->result();
    }
    
    function update_outbound_data($url,$msgid,$status,$knlstatus){
        
        if($url <> null){
            
            $set_url =" dlr_url='$url',";
        }
        
        if($knlstatus <> null){
            
            $set_knl =",kannel_status='$knlstatus'";
        }
        return $this->kannel->query("UPDATE tbl_shortcode_outbound SET $set_url lastupdate='".date('Y-m-d H:i:s')."',dse_status='$status' $set_knl WHERE dse_messageid='$msgid'");
    }
    
	function update_outbound_batch_data($data,$categ){
        
        $this->kannel->trans_start();
        
        foreach($data as $key=>$value){
			
			if($categ == 'outbound'){
				
                $this->kannel->query("UPDATE tbl_shortcode_outbound SET dlr_url='".$value['dlr_url']."',lastupdate='".date('Y-m-d H:i:s')."',dse_status='".$value['dse_status']."',kannel_status='".$value['kannel_status']."' WHERE dse_messageid='".$value['dse_messageid']."'");
            }
            
            if($categ == 'dlr_report'){
                
                $this->kannel->query("UPDATE tbl_shortcode_outbound SET lastupdate='".date('Y-m-d H:i:s')."',dse_status='".$value['dse_status']."' WHERE dse_messageid='".$value['dse_messageid']."'");
            }
        }
      
        $this->kannel->trans_complete();
        
        return $this->kannel->trans_status();
    }
	
	function update_timwe_batch_data($data,$categ){
        
        $this->kannel->trans_start();
        
        foreach($data as $key=>$value){
            
            if($categ == 'chargable'){
                
                $this->kannel->query("UPDATE tbl_shortcode_outbound SET dlr_messageid='".$value['dlr_messageid']."',lastupdate='".date('Y-m-d H:i:s')."',dse_status='".$value['dse_status']."' WHERE dse_messageid='".$value['dse_messageid']."'");
            }
            
            if($categ == 'unchargable'){
                
                $this->kannel->query("UPDATE timwe_mo SET status='101',dlr_messageid='".$value['dlr_messageid']."' WHERE transactionUUID='".$value['transactionUUID']."'");
            }
        }
        
        $this->kannel->trans_complete();
        
        return $this->kannel->trans_status();
    }
	
    function update_inbound_data($id,$status){
        
        
        return $this->kannel->query("UPDATE tbl_shortcode_inbound SET lastupdate='".date('Y-m-d H:i:s')."',status='$status' WHERE id='$id'");
    }
	
	function update_inbound_batch_data($data){
        
        
        $this->kannel->trans_start();
        
        foreach($data as $key=>$value){
            
            $this->kannel->query("UPDATE tbl_shortcode_inbound SET lastupdate='".date('Y-m-d H:i:s')."',status='".$value['status']."' WHERE id='".$value['id']."'");
        }
        
        $this->kannel->trans_complete();
        
        return $this->kannel->trans_status();
    }
}