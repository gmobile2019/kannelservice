<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Monitor_model extends CI_Model
{
    private $kannel;
    
    public function __construct()
	{
		parent::__construct();
		$this->kannel=$this->load->database('kannel',TRUE);//load main database configuration
        }
   
    function request_inbound_data($rec,$mno,$startdate,$enddate,$limit){
        
        if($mno <> null){
            
            $where .=" AND mno='$mno'";
        }
        
        if($rec <> null){
            
            $where .=" AND source_addr LIKE '%$rec%'";
        }
        
        if($startdate <> null){
            
            $where .=" AND arrivaltimestamp >='$startdate 00:00:00'";
        }
        
        if($enddate <> null){
            
            $where .=" AND arrivaltimestamp <='$enddate 23:59:59'";
        }
        
        if($limit <> null){
            
            $limit =" LIMIT 0,$limit";
        }
        
        return $this->kannel->query("SELECT id,keyword,source_addr,conn_id,lastupdate,arrivaltimestamp,msg_category,mno,lang,(
                    CASE 
                        WHEN status = '101' THEN 'Synchronized'
                        ELSE 'Pending'
                    END ) AS syncstatus FROM tbl_shortcode_inbound WHERE id is not null $where ORDER BY id DESC $limit")->result();
    }
    
    function request_inbound_unknown_data($rec,$mno,$startdate,$enddate,$limit){
        
        if($mno <> null){
            
            $where .=" AND mno='$mno'";
        }
        
        if($rec <> null){
            
            $where .=" AND source_addr LIKE '%$rec%'";
        }
        
        if($startdate <> null){
            
            $where .=" AND arrivaltimestamp >='$startdate 00:00:00'";
        }
        
        if($enddate <> null){
            
            $where .=" AND arrivaltimestamp <='$enddate 23:59:59'";
        }
        
        if($limit <> null){
            
            $limit =" LIMIT 0,$limit";
        }
        
        return $this->kannel->query("SELECT id,keyword,source_addr,conn_id,lastupdate,arrivaltimestamp,msg_category,mno,lang,"
                . "(
                    CASE 
                        WHEN status = '101' THEN 'Synchronized'
                        ELSE 'Pending'
                    END ) AS syncstatus FROM tbl_shortcode_unknown WHERE id is not null $where ORDER BY id DESC $limit")->result();
    }
    
    function request_outbound_data($rec,$mno,$startdate,$enddate,$limit){
        
        if($mno <> null){
            
            $where .=" AND sendercode='$mno'";
        }
        
        if($rec <> null){
            
            $where .=" AND dse_recipient LIKE '%$rec%'";
        }
        
        if($startdate <> null){
            
            $where .=" AND dse_timestamp >='$startdate 00:00:00'";
        }
        
        if($enddate <> null){
            
            $where .=" AND dse_timestamp <='$enddate 23:59:59'";
        }
        
        if($limit <> null){
            
            $limit =" LIMIT 0,$limit";
        }
        
        return $this->kannel->query("SELECT id,dse_messageid,dse_timestamp,dse_recipient,"
                . "(
                    CASE 
                        WHEN dse_status = '100' THEN 'Kannel Pending'
                        WHEN dse_status = '101' THEN 'Dlr Pending'
                        WHEN dse_status = '102' THEN 'Dse Pending'
                        WHEN dse_status = '103' THEN 'Complete'
                        ELSE 'Unknown'
                    END ) AS status,kannel_status,(
                    CASE 
                        WHEN dlr_status = '1' THEN 'Delivery Success'
                        WHEN dlr_status = '16' THEN 'Rejected'
                        WHEN dlr_status = '4' THEN 'Message Buffered'
                        WHEN dlr_status = '2' THEN 'Delivery Failure'
                        WHEN dlr_status = '8' THEN 'Smsc Submit'
                        ELSE 'Unknown'
                    END ) AS dlrstatus,dlr_timestamp,(
                    CASE 
                        WHEN sendercode = '101' THEN 'Airtel'
                        ELSE 'Unknown'
                    END ) AS sender,lastupdate,dlr_timestamp FROM tbl_shortcode_outbound WHERE id is not null $where ORDER BY id DESC $limit")->result();
    }
}