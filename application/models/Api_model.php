<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Api_model extends CI_Model
{
    private $kannel;
    
    public function __construct()
	{
		parent::__construct();
		$this->kannel=$this->load->database('kannel',TRUE);//load main database configuration
        }
    
    function save_incoming_request($data){
        
        return $this->kannel->insert('tbl_shortcode_inbound',$data);
    }
    
	function save_unknown($data){
        
        return $this->kannel->insert('tbl_shortcode_unknown',$data);
    }
	
    function check_account($source_address,$status,$msg_category){
        
        if($msg_category =='101'){
            
            $msg=$this->kannel->query("SELECT id,msg_category FROM tbl_shortcode_inbound WHERE source_addr LIKE '%".substr($source_address,-9)."' ORDER BY id DESC LIMIT 0,1")->row();
            
            if($msg->msg_category == '101'){
                return 'registered';
            }else{
                return null;
            }
        }
        
        if($msg_category =='102'){
            
            $msg=$this->kannel->query("SELECT id,msg_category FROM tbl_shortcode_inbound WHERE source_addr LIKE '%".substr($source_address,-9)."' ORDER BY id DESC LIMIT 0,1")->row();
            
            if($msg->msg_category == '102'){
                return 'unregistered';
            }else{
                return null;
            }
        }
        
    }
    
    function check_access_credentials($username,$password){
        
        $data=$this->kannel->query("SELECT id FROM api_credentials WHERE username='$username' AND password='$password' AND status='1'")->row();
        
        if($data <> null){
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    function get_response_details($errorcode){
        
        return $this->kannel->query("SELECT id,code,status,description FROM api_response_codes WHERE code='$errorcode'")->row();
    }
    
    function save_outbound_request($data){
        
        return $this->kannel->insert('tbl_shortcode_outbound',$data);
    }
    
    function update_dlr_smsc($data,$msgid){
        
        return $this->kannel->update('tbl_shortcode_outbound',$data,array('dse_messageid'=>$msgid));
    }
	
	function save_incoming_request_timwe($request,$ip){
        
        $data=array(
            'request'=>$request,
            'source'=>$ip,
            'createdon'=>date('Y-m-d H:i:s'),
        );
        
        return $this->kannel->insert('api_requests',$data);
    }
    
    function save_timwe_mo($mo,$inbound){
        
        $this->kannel->trans_start();
        
        $this->kannel->insert('timwe_mo',$mo);
        $this->kannel->insert('tbl_shortcode_inbound',$inbound);
        $this->kannel->trans_complete();
        
        return $this->kannel->trans_status();
    }
    
    function save_timwe_dn($data,$dn,$transactionUUID){
        
        $this->kannel->trans_start();
        
        $this->kannel->insert('timwe_dn',$data);
        $this->kannel->update('tbl_shortcode_outbound',$dn,array('dlr_messageid'=>$transactionUUID));
        $this->kannel->trans_complete();
        
        return $this->kannel->trans_status();
    }
    
    function save_timwe_cn($data,$cn,$transactionUUID){
        
        $this->kannel->trans_start();
        
        $this->kannel->insert('timwe_cn',$data);
        $this->kannel->update('tbl_shortcode_outbound',$cn,array('dlr_messageid'=>$transactionUUID));
        $this->kannel->trans_complete();
        
        return $this->kannel->trans_status();
    }
}