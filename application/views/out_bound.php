<script type="text/javascript">
    $(document).ready(function(){
        
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
           
            $('a').find('span#pdf').css({
                                            color: '#000000'
                                        });
        
    });
</script>

<div id="display_data" >
    <div style="padding:5px;text-align: center" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 

                        $attributes = array('class' => 'form-inline','role'=>'form');
                        echo form_open('Monitor/outbound_requests',$attributes); 
                    ?>
           <div class="form-group ">
                <label class="sr-only" for="recipient"></label>
                    <input type="text" class="form-control" name="recipient" id="recipient" placeholder="Recipient" value="<?php echo $recipient; ?>" />
           </div>
           <div class="form-group">
                <label class="sr-only" for="mno"></label>
                <select name="mno" id="status" class="form-control " >
                    <option value=""></option>
                    <option value="101" <?php echo $mno == '101'?'selected="selected"':''; ?>>Airtel</option>
                </select>
            </div>
            <div class="form-group">
                <label class="sr-only" for="start"></label>
                    <input type="text" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
           </div>
            <div class="form-group">
                <label class="sr-only" for="end"></label>
                    <input type="text" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
           </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Search</button>
           </div>


        <?php echo form_close(); ?>
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:150px">S/NO</th>
                <th style="text-align:center;width400px">Message Id</th>
                <th style="text-align:center;width:300px">Recipient</th>
                <th style="text-align:center;width:300px">Sender Code</th>
                <th style="text-align:center;width:300px">Dse Status</th>
                <th style="text-align:center;width:400px">Kannel Status</th>
                <th style="text-align:center;width:400px">Dlr Status</th>
                <th style="text-align:center;width:400px">Arrival</th>
                <th style="text-align:center;width:400px">Dlr Timestamp</th>
                <th style="text-align:center;width:400px">Last Update</th>
            </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                $i=1;
                foreach($data as $key=>$value){ 
                   
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->dse_messageid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->dse_recipient; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->sender; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->kannel_status; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->dlrstatus; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->dse_timestamp; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->dlr_timestamp; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->lastupdate; ?></td>
                    </tr>  
                <?php } 
                }else{ ?>
            <tr>
                <td colspan="10" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>

