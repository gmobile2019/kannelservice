<!DOCTYPE html>
<html lang="en">
<head>
<title>Monitor</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>styles/fontawesome/css/font-awesome.min.css">
<link rel="icon" href="<?php echo base_url() ?>images/dse_ico.jpg" type="image/x-icon">

<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/sidebarmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/chart.bundle.js" language="javscript"></script>
<style type="text/css">
    .background {
        background-color: #006666;
    }
    
    div.scrollmenu {
    overflow: auto;
    white-space: nowrap;
}

div.scrollmenu a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px;
    text-decoration: none;
}

div.scrollmenu a:hover {
    background-color: #777;
}

.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #006666;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: underline;
    font-size: 100%;
    color: whitesmoke;
    display: block;
    transition: 0.3s
}

.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
    background-color: #777;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    margin-left: 50px;
}

#main {
    transition: margin-left .5s;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.side_main_menu span {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 100%;
    display: block;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $('.blink').blink({delay:1000});
        
        $('div.side_main_menu').find('span.openbtn').each(function(){
            
            $(this).click(function(){
            
                var id=$(this).attr('id');
                document.getElementById("mySidenav"+id).style.width = "200px";
                document.getElementById("main").style.marginLeft = "200px";
            });
        });
        
        $('div.side_main_menu').find('a.closebtn').each(function(){
            
            $(this).click(function(){
            
                var id=$(this).attr('id');
                document.getElementById("mySidenav"+id).style.width = "0";
                document.getElementById("main").style.marginLeft = "0";
            });
        });
    });
    
</script>
</head>
<body id="main">
    <div class="scrollmenu background">
        <span style="padding-left: 10px"><img src="<?php echo base_url()?>images/dse_ico.jpg" alt="DSE LOGO" width="35px" style="border-radius:2px"/></span>
        <span style="padding-left: 80px"></span>
        <a href="<?php echo base_url() ?>index.php/Monitor/in_bound">In Bound Requests</a>
        <a href="<?php echo base_url() ?>index.php/Monitor/outbound_requests">Out Bound Requests</a>
        <a href="<?php echo base_url() ?>index.php/Monitor/in_bound_unknown">In Bound Unknown</a>
        <a href="<?php echo base_url() ?>index.php/Monitor/report">Reports</a>
        <span class="blink" style="padding-left: 150px;color:whitesmoke;font-style: italic;font-weight: bolder">DSE Monitor</span>
    </div>
    
      <div class="container-fluid">
          <div id="body-bar" class="row well">
            <div class="col-xs-12 col-sm-12 col-lg-6">
                
            </div>
        </div>
        <div class="row">
           
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div style="padding-left:0px" class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-primary"><?php echo $title; ?></button>
                </div>
                 <?php $this->load->view($content); ?>
            </div>
            
        </div>
    </div>
</body>
</html>